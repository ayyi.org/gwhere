#!/bin/sh

libtoolize --automake
aclocal
autoheader -Wall
automake --gnu --add-missing -Wall
autoconf

