GWhere
------

The original project hasn't been updated for many years and no longer compiles.
This repository contains some quick fixes to allow it to be used on a modern
system.

Perhaps controversially, internationalisation and support for MS Windows is removed.

Please see the orginal [README](README).
